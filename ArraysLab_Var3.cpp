﻿#include <iostream>
#include <ctime>
#include <algorithm>

void initialize_data(int*, int*);
bool check_input_data(int, int);

void initialize_array(int*, int);
void show_array(int*, int);
void replase_array_elements(int*, int, int);
void replase_array_elements_with_reverse(int*, int, int);

int main()
{
	setlocale(0, "");

	int array_size;
	int k;

	initialize_data(&array_size, &k);

	while (!check_input_data(array_size, k))
	{
		std::cout << "Данные введены неверно. Повторите ввод \n \n \n";

		initialize_data(&array_size, &k);
	}

	int* arr = new int[array_size];

	initialize_array(arr, array_size);
	show_array(arr, array_size);

	//replase_array_elements(arr, array_size, k);
	replase_array_elements_with_reverse(arr, array_size, k);

	show_array(arr, array_size);

	delete[] arr;
}

void initialize_array(int* a, int size)
{
	srand(time(0));

	for (int i = 0; i < size; i++)
		* (a + i) = rand() % 50 - 10;
}

void show_array(int* a, int size)
{
	std::cout << "\nArray:\n";

	for (int i = 0; i < size; i++)
		std::cout << *(a + i) << " ";
}

void initialize_data(int* array_size, int* k)
{
	int _size = 0;
	int _k = 0;

	std::cout << "Размер массива = ";
	std::cin >> _size;

	std::cout << "Смещение элементов K = ";
	std::cin >> _k;

	*array_size = _size;
	*k = _k;
}

bool check_input_data(int array_size, int k)
{
	if (array_size <= 0)
	{
		std::cout << "Array size must be > 0" << "\n";

		return false;
	}

	if (k >= array_size)
	{
		std::cout << "K must be < array size" << "\n";

		return false;
	}

	return true;
}

//сдвиг элементов массива с использованием отдельного массива
void replase_array_elements(int* a, int size, int dist)
{
	int* new_array = new int[size];
	dist -= size;
	if (dist < 0)
		dist *= -1;

	for (int i = 0; i < size; i++)
	{
		auto index = (i + dist) % size;

		new_array[i] = *(a + index);
	}

	for (int i = 0; i < size; i++)
		* (a + i) = new_array[i];

	delete[] new_array;
}

//сдвиг элементов массива с использованием std::reverse
void replase_array_elements_with_reverse(int* a, int size, int dist)
{
	std::reverse(&a[0], &a[dist]);
	std::reverse(&a[dist], &a[size - dist]);
	std::reverse(&a[0], &a[size]);
}